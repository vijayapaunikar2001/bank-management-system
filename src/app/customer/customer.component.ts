import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

   customerForm:FormGroup;
   selectedIndex3='';
   isEditBtnClicked3="no";
   customerList:any=[];

   isSubmitted = true;
   constructor(private formBuilder:FormBuilder) 
   { 
       this.customerForm=this.formBuilder.group(
         {
             cname:['',[Validators.required]],
             cemail:['',[Validators.required]],
             ccontact:['',[Validators.required]],
             caadhar:['',[Validators.required]],
         }
 
       )
   }
   
     submit3(){
       this.isSubmitted = true;
     if(this.customerForm.valid)
     {
       console.log('submit',this.customerForm.value);
       this.customerList.push(this.customerForm.value);
       
       // Code to add data in localstorage
       localStorage.setItem('doctorList',JSON.stringify(this.customerList));
       
       Swal.fire({
         position: 'top-end',
         icon: 'success',
         title: 'Your details has been Submitted',
         showConfirmButton: false,
         timer: 1500
       });
       this.customerForm.reset();
       this.isSubmitted = false;
     }
    }
       
    clear3(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.customerForm.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit3(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked3="yes";
      this.selectedIndex3 = idx;
      this.customerForm.patchValue(
        {
          cname: this.customerList[idx].cname,
          cemail: this.customerList[idx].cemail,
          caadhar: this.customerList[idx].caadhar,
          ccontact: this.customerList[idx].ccontact,
          
        }
      )
    }
  
    delete3(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.customerList.splice(idx,1);
          //code to delete data in localstorage
      localStorage.setItem('customerList',JSON.stringify(this.customerList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update3(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.customerList[this.selectedIndex3].cname= this.customerForm.value.cname;
      this.customerList[this.selectedIndex3].cemail = this.customerForm.value.cemail;
      this.customerList[this.selectedIndex3].ccontact = this.customerForm.value.ccontact;
      this.customerList[this.selectedIndex3].caadhar = this.customerForm.value.caadhar;
      
      //code to update data in localstorage
      localStorage.setItem('customerList',JSON.stringify(this.customerList))

      this.customerForm.reset();
      this.isEditBtnClicked3="no";
    }
    ngOnInit(): void {
        //code to read data in localstorage
      let data = localStorage.getItem('customerList');
      this.customerList = JSON.parse(data || '');
    }
  
    
}


