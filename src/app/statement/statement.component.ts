import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-statement',
  templateUrl: './statement.component.html',
  styleUrls: ['./statement.component.scss']
})
export class StatementComponent implements OnInit {

  
  statementform:FormGroup;
  selectedIndex5='';
  isEditBtnClicked5="no";
  statementList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.statementform=this.formBuilder.group(
        {
            tname:['',[Validators.required]],
            temail:['',[Validators.required]],
            tcontact:['',[Validators.required]],
            taadhar:['',[Validators.required]],
        }

      )
  }
  
    submit5(){
      this.isSubmitted = true;
    if(this.statementform.valid)
    {
      console.log('submit',this.statementform.value);
      this.statementList.push(this.statementform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('statementList',JSON.stringify(this.statementList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.statementform.reset();
      this.isSubmitted = false;
    }
      
    }
   clear5(){
     const swalWithBootstrapButtons = Swal.mixin({
       customClass: {
         confirmButton: 'btn btn-success',
         cancelButton: 'btn btn-danger'
       },
       buttonsStyling: false
     })
     
     swalWithBootstrapButtons.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonText: 'Yes, cancel it!',
       cancelButtonText: 'No, cancel!',
       reverseButtons: true
     }).then((result) => {
       if (result.isConfirmed) {
         swalWithBootstrapButtons.fire(
           'Deleted!',
           'Your details has been deleted.',
           'success'
         )
         this.statementform.reset();
       } else if (
         /* Read more about handling dismissals below */
         result.dismiss === Swal.DismissReason.cancel
       ) {
         swalWithBootstrapButtons.fire(
           'Cancelled',
           'Your details are safe :)',
           'error'
         )
       }
     })
     
     
   }
   edit5(idx:any){
     Swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes'
     }).then((result) => {
       if (result.isConfirmed) {
         Swal.fire(
           'update!',
           'You can update.',
           
         )
       }
     })
     this.isEditBtnClicked5="yes";
     this.selectedIndex5 = idx;
     this.statementform.patchValue(
       {
         tname: this.statementList[idx].tname,
         temail: this.statementList[idx].temail,
         taadhar: this.statementList[idx].taadhar,
         tcontact: this.statementList[idx].tcontact,
         
       }
     )
   }
 
   delete5(idx:any){
     const swalWithBootstrapButtons = Swal.mixin({
       customClass: {
         confirmButton: 'btn btn-success',
         cancelButton: 'btn btn-danger'
       },
       buttonsStyling: false
     })
     
     swalWithBootstrapButtons.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonText: 'Yes, delete it!',
       cancelButtonText: 'No, cancel!',
       reverseButtons: true
     }).then((result) => {
       if (result.isConfirmed) {
         swalWithBootstrapButtons.fire(
           'Deleted!',
           'Your file has been deleted.',
           'success'
         )
         this.statementList.splice(idx,1);
          //code to update data in localstorage
    localStorage.setItem('statementList',JSON.stringify(this.statementList))
       } else if (
         /* Read more about handling dismissals below */
         result.dismiss === Swal.DismissReason.cancel
       ) {
         swalWithBootstrapButtons.fire(
           'Cancelled',
           'Your imaginary file is safe :)',
           'error'
         )
       }
     })
     
   }
 
   update5(){
     Swal.fire({
       title: 'Are you sure, you want to update?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, update it!'
     }).then((result) => {
       if (result.isConfirmed) {
         Swal.fire(
           'Updated!',
           'Your details has been updated.',
           'success'
         )
       }
     })
     this.statementList[this.selectedIndex5].tname= this.statementform.value.tname;
     this.statementList[this.selectedIndex5].temail= this.statementform.value.temail;
     this.statementList[this.selectedIndex5].tcontact= this.statementform.value.tcontact;
     this.statementList[this.selectedIndex5].taadhar= this.statementform.value.taadhar;

      //code to update data in localstorage
    localStorage.setItem('statementList',JSON.stringify(this.statementList))

     this.statementform.reset();
     this.isEditBtnClicked5="no";
   }
   ngOnInit(): void {
       //code to read data in localstorage
      let data = localStorage.getItem('statementList');
      this.statementList = JSON.parse(data || '');
   }
 
   
}


