import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Miniproject1';
  // Login
  loginemail="";
  password="";
  selectedIndex="";
  isEditBtnClicked="no";
  loginList:any=[]

  submit(){
    let login ={
      loginemail:this.loginemail,
      password:this.password
    }
    this.loginList.push(login)
    this.clear()
    console.log("Successfully Logined",login);
  }

  clear(){
    this.loginemail="";
    this.password="";
    console.log("Cancelled Login")
  }

  
  // Register
  registerList:any=[];
  rName="";
  remail="";
  rcontact="";
  raadhar="";
  selectedIndex1="";
  isEditBtnClicked1="no";

  submit1(){
    let register ={
      rName:this.rName,
      remail:this.remail,
      rcontact:this.rcontact,
      raadhar:this.raadhar,
    }
    this.registerList.push(register)
    this.clear1()
    console.log("Successfully Registered",register);
    
  }

  clear1(){
    this.rName="";
    this.remail="";
    this.rcontact="";
    this.raadhar="";
    console.log("Successfully Cancelled");
  }
  
  

  // Employee
  empList:any=[];
  ename="";
  eemail="";
  econtact="";
  eaadhar="";
  selectedIndex2="";
  isEditBtnClicked2="no";

  submit2(){
    let emp ={
      ename:this.ename,
      eemail:this.eemail,
      econtact:this.econtact,
      eaadhar:this.eaadhar,
    }
    this.empList.push(emp)
    this.clear2()
    
  }

  clear2(){
    this.ename="";
    this.eemail="";
    this.econtact="";
    this.eaadhar="";
  }
  edit2(idx:any){
    this.isEditBtnClicked2="yes";
    this.selectedIndex2 = idx;
    this.ename=this.empList[idx].ename;
    this.eemail=this.empList[idx].eemail;
    this.econtact=this.empList[idx].econtact;
    this.eaadhar=this.empList[idx].eaadhar;
  }

  delete2(idx:any){
    this.empList.splice(idx,1);
  }

  update2(){
    this.empList[this.selectedIndex2].ename = this.ename;
    this.empList[this.selectedIndex2].eemail = this.eemail;
    this.empList[this.selectedIndex2].econtact = this.econtact;
    this.empList[this.selectedIndex2].eaadhar = this.eaadhar;
    
    this.clear2();
    this.isEditBtnClicked2="no";
  }

// Customer
customerList:any=[];
cname="";
cemail="";
ccontact="";
caadhar="";
selectedIndex3="";
isEditBtnClicked3="no";

submit3(){
  let custom ={
    cname:this.cname,
    cemail:this.cemail,
    ccontact:this.ccontact,
    caadhar:this.caadhar,
  }
  this.customerList.push(custom)
  this.clear3()
  
}

clear3(){
  this.cname="";
  this.cemail="";
  this.ccontact="";
  this.caadhar="";
}
edit3(idx:any){
  this.isEditBtnClicked3="yes";
  this.selectedIndex3 = idx;
  this.cname=this.customerList[idx].cname;
  this.cemail=this.customerList[idx].cemail;
  this.ccontact=this.customerList[idx].ccontact;
  this.caadhar=this.customerList[idx].caadhar;
}

delete3(idx:any){
  this.customerList.splice(idx,1);
}

update3(){
  this.customerList[this.selectedIndex3].cname = this.cname;
  this.customerList[this.selectedIndex3].cemail = this.cemail;
  this.customerList[this.selectedIndex3].ccontact = this.ccontact;
  this.customerList[this.selectedIndex3].caadhar = this.caadhar;
  
  this.clear3();
  this.isEditBtnClicked3="no";
}
// Staff
staffList:any=[];
  sname="";
  semail="";
  scontact="";
  saadhar="";
  selectedIndex4="";
  isEditBtnClicked4="no";

  submit4(){
    let staff ={
      sname:this.sname,
      semail:this.semail,
      scontact:this.scontact,
      saadhar:this.saadhar,
    }
    this.staffList.push(staff)
    this.clear4()
    
  }

  clear4(){
    this.sname="";
    this.semail="";
    this.scontact="";
    this.saadhar="";
  }
  edit4(idx:any){
    this.isEditBtnClicked4="yes";
    this.selectedIndex4 = idx;
    this.sname=this.staffList[idx].sname;
    this.semail=this.staffList[idx].semail;
    this.scontact=this.staffList[idx].scontact;
    this.saadhar=this.staffList[idx].saadhar;
  }

  delete4(idx:any){
    this.staffList.splice(idx,1);
  }

  update4(){
    this.staffList[this.selectedIndex4].sname = this.sname;
    this.staffList[this.selectedIndex4].semail = this.semail;
    this.staffList[this.selectedIndex4].scontact = this.scontact;
    this.staffList[this.selectedIndex4].saadhar = this.saadhar;
    
    this.clear4();
    this.isEditBtnClicked4="no";
  }
  // Statements
  statementList:any=[];
  tname="";
  temail="";
  tcontact="";
  taadhar="";
  selectedIndex5="";
  isEditBtnClicked5="no";

  submit5(){
    let state ={
      tname:this.tname,
      temail:this.temail,
      tcontact:this.tcontact,
      taadhar:this.taadhar,
    }
    this.statementList.push(state)
    this.clear5()
    
  }

  clear5(){
    this.tname="";
    this.temail="";
    this.tcontact="";
    this.taadhar="";
  }
  edit5(idx:any){
    this.isEditBtnClicked5="yes";
    this.selectedIndex5 = idx;
    this.tname=this.statementList[idx].tname;
    this.temail=this.statementList[idx].temail;
    this.tcontact=this.statementList[idx].tcontact;
    this.taadhar=this.statementList[idx].taadhar;
  }

  delete5(idx:any){
    this.statementList.splice(idx,1);
  }

  update5(){
    this.statementList[this.selectedIndex5].tname = this.tname;
    this.statementList[this.selectedIndex5].temail = this.temail;
    this.statementList[this.selectedIndex5].tcontact = this.tcontact;
    this.statementList[this.selectedIndex5].taadhar = this.taadhar;
    
    this.clear5();
    this.isEditBtnClicked5="no";
  }


  
}

