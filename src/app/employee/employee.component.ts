import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit 
  
{
  employeeform:FormGroup;
  selectedIndex2='';
  isEditBtnClicked2="no";
  empList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.employeeform=this.formBuilder.group(
        {
            ename:['',[Validators.required]],
            eemail:['',[Validators.required]],
            econtact:['',[Validators.required]],
            eaadhar:['',[Validators.required]],
        }

      )
  }
  
    submit2(){
      this.isSubmitted = true;
    if(this.employeeform.valid)
    {
      console.log('submit',this.employeeform.value);
      this.empList.push(this.employeeform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('empList',JSON.stringify(this.empList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.employeeform.reset();
      this.isSubmitted = false;
    }
   }
      
   clear2(){
     const swalWithBootstrapButtons = Swal.mixin({
       customClass: {
         confirmButton: 'btn btn-success',
         cancelButton: 'btn btn-danger'
       },
       buttonsStyling: false
     })
     
     swalWithBootstrapButtons.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonText: 'Yes, cancel it!',
       cancelButtonText: 'No, cancel!',
       reverseButtons: true
     }).then((result) => {
       if (result.isConfirmed) {
         swalWithBootstrapButtons.fire(
           'Deleted!',
           'Your details has been deleted.',
           'success'
         )
         this.employeeform.reset();
       } else if (
         /* Read more about handling dismissals below */
         result.dismiss === Swal.DismissReason.cancel
       ) {
         swalWithBootstrapButtons.fire(
           'Cancelled',
           'Your details are safe :)',
           'error'
         )
       }
     })
     
     
   }
   edit2(idx:any){
     Swal.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes'
     }).then((result) => {
       if (result.isConfirmed) {
         Swal.fire(
           'update!',
           'You can update.',
           
         )
       }
     })
     this.isEditBtnClicked2="yes";
     this.selectedIndex2 = idx;
     this.employeeform.patchValue(
       {
         ename: this.empList[idx].ename,
         eemail: this.empList[idx].eemail,
         eaadhar: this.empList[idx].eaadhar,
         econtact: this.empList[idx].econtact,
         
       }
     )
   }
 
   delete2(idx:any){
     const swalWithBootstrapButtons = Swal.mixin({
       customClass: {
         confirmButton: 'btn btn-success',
         cancelButton: 'btn btn-danger'
       },
       buttonsStyling: false
     })
     
     swalWithBootstrapButtons.fire({
       title: 'Are you sure?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonText: 'Yes, delete it!',
       cancelButtonText: 'No, cancel!',
       reverseButtons: true
     }).then((result) => {
       if (result.isConfirmed) {
         swalWithBootstrapButtons.fire(
           'Deleted!',
           'Your file has been deleted.',
           'success'
         )
         this.empList.splice(idx,1);
         //code to delete data in localstorage
      localStorage.setItem('empList',JSON.stringify(this.empList))
       } else if (
         /* Read more about handling dismissals below */
         result.dismiss === Swal.DismissReason.cancel
       ) {
         swalWithBootstrapButtons.fire(
           'Cancelled',
           'Your imaginary file is safe :)',
           'error'
         )
       }
     })
     
   }
 
   update2(){
     Swal.fire({
       title: 'Are you sure, you want to update?',
       text: "You won't be able to revert this!",
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Yes, update it!'
     }).then((result) => {
       if (result.isConfirmed) {
         Swal.fire(
           'Updated!',
           'Your details has been updated.',
           'success'
         )
       }
     })
     this.empList[this.selectedIndex2].ename= this.employeeform.value.ename;
     this.empList[this.selectedIndex2].econtact= this.employeeform.value.econtact;
     this.empList[this.selectedIndex2].eaadhar= this.employeeform.value.eaadhar;
     this.empList[this.selectedIndex2].eemail= this.employeeform.value.eemail;
    //code to update data in localstorage
    localStorage.setItem('empList',JSON.stringify(this.empList))
     
     this.employeeform.reset();
     this.isEditBtnClicked2="no";
   }
   ngOnInit(): void {
       //code to read data in localstorage
      let data = localStorage.getItem('empList');
      this.empList = JSON.parse(data || '');
   }
 
   
}


